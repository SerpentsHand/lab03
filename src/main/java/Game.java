import java.util.*;

public class Game {
    
    private final List<Frame> frames;
    
    public Game() {
        frames = new ArrayList<>();
        frames.add(new Frame());
    }
    
    public void roll(int pins) {
        if(frames.get(frames.size() - 1).isFinished()) {
            frames.add(new Frame());
        }
        frames.get(frames.size() - 1).roll(pins);
    }
    
    public int score() {
        int score = 0;
        for(int i = 0; i < 10; i++) {
            Frame frame = frames.get(i);
            score += frame.score();
            if(frame.isSpare() || frame.isStrike()) {
                score += frames.get(i + 1).score(0);
            }
            if(frame.isStrike()) {
                score += !frames.get(i + 1).isStrike() ? frames.get(i + 1).score(1) : frames.get(i + 2).score(0);
            }
        }
        return score;
    }
}
