class Frame {
        
    private Integer[] score = new Integer[] {null, null};
    
    boolean isFinished() {
        return score[0] != null && score[1] != null;
    }
    
    boolean isStrike() {
        return score[0] == 10 && score[1] == 0;
    }
    
    boolean isSpare() {
        return !isStrike() && score[0] + score[1] == 10;
    }
    
    void roll(int pins) {
        if(score[0] == null) {
            score[0] = pins;
            if(pins == 10) {
                roll(0);
            }
        }
        else if(score[1] == null) {
            score[1] = pins;
        }
        else {
            throw new IllegalStateException();
        }
    }
    
    int score() {
        return score[0] + score[1];
    }
    
    int score(int ind) {
        return score[ind];
    }
}
